<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\SatuanController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.auth.login');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth']], function(){
    Route::get('/dashboard', DashboardController::class)->name('dashboard');


    Route::group(['prefix' => 'user', 'as' => 'user.'], function(){
        Route::get('/', [UserController::class, 'index'])->name('index');
        Route::get('/index2', [UserController::class, 'index2'])->name('index2');
    });

    Route::group(['prefix' => 'satuan', 'as' => 'satuan.'], function(){
        Route::get('/', [SatuanController::class, 'index'])->name('index');
    });

    Route::resource('products',ProductController::class);
    Route::resource('roles', RoleController::class);
});


