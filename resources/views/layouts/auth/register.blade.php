@extends('layouts.auth.app', ['title' => 'Register'])

@section('content')
<div class="card card-primary">
    <div class="card-header"><h4>Register</h4></div>

    <div class="card-body">
      <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form-group">
            <label for="first_name">Name</label>
            <input id="name" name="name" type="name" class="form-control @error('name') is-invalid @enderror">
            @error('name')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="first_name">Email</label>
            <input id="email" name="email" type="email" class="form-control @error('email') is-invalid @enderror">
            @error('email')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="first_name">Password</label>
            <input id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror">
            @error('password')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="first_name">Password Confirmation</label>
            <input id="password_confirm" name="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror">
            @error('password_confirmation')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-lg btn-block">
            Register
          </button>
        </div>
      </form>
    </div>
  </div>
  <div class="mt-5 text-muted text-center">
        Already Have Account?  <a href="">Sign In Now</a>
  </div>
@endsection

