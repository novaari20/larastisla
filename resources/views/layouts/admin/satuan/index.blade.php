@extends('layouts.admin.app', ['title' => 'Dashboard Satuan'])

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Table Satuan</h4>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                      <table class="table table-striped table-md">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Satuan</th>
                                <th>Qty</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </div>
                  </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    var table = $('.table-md').DataTable({

    });
</script>
@endpush
