<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="index.html">Stisla</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">St</a>
      </div>
      <ul class="sidebar-menu">
          <li class="menu-header">Dashboard</li>
          <li class="nav-item dropdown">
            <a href="{{ route('admin.dashboard') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
          </li>
          <li class="active"><a class="nav-link" href="{{ route('admin.user.index') }}"><i class="far fa-user"></i> <span>User</span></a></li>
          <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-dumpster"></i> <span>Master</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link" href="{{ route('admin.satuan.index') }}">Master Satuan</a></li>
              <li><a class="nav-link" href="#">Transparent Sidebar</a></li>
              <li><a class="nav-link" href="#">Top Navigation</a></li>
            </ul>
          </li>
        </ul>
    </aside>
  </div>
