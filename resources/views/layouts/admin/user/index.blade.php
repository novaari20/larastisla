@extends('layouts.admin.app', ['title' => 'Dashboard Users'])


@section('content')
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h4>Table Users</h4>
                </div>
                <div class="card-body p-0">
                  <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
                    </li>
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active table-responsive" id="home" role="tabpanel" aria-labelledby="home-tab">
                     <table class="table table-striped table1" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>email</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                     </table>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                     <table class="table table-striped table2" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created At</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                     </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
@endsection
@push('scripts')
    <script>
        var table = $('.table1').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            searching: false,
            ajax: {
                url: "{{ route('admin.user.index') }}",
                dataType: "json",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'action', name: 'action'},
            ],
        });

        var table2 = $('.table2').DataTable({
            processing: true,
            serverSide: true,
            pageLength: 25,
            ajax: {
                url: "{{ route('admin.user.index2') }}",
                dataType: "json",
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action'},
            ],
        });
    </script>
@endpush
